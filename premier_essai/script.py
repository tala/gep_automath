def arrange_blocs(blocs_1, blocs_2):
    arrangement = r"""
\begin{{minipage}}[c]{{0.4\textwidth}}
    \begin{{scratch}}
        {blocs_1}
    \end{{scratch}}
\end{{minipage}}
\begin{{minipage}}[c]{{0.15\textwidth}}
    \begin{{tikzpicture}}[->]
        \draw [>=stealth,blue, ultra thick] (0,0) -- +(1.5,0);
        \end{{tikzpicture}}
\end{{minipage}}
\begin{{minipage}}[c]{{0.4\textwidth}}
    \begin{{scratch}}
        {blocs_2}
    \end{{scratch}}
\end{{minipage}}
    """.format(
        blocs_1=blocs_1, blocs_2=blocs_2
    )
    return arrangement


def blocs_addition(a, b):
    blocs_op = r"\blockinit{quand \greenflag est cliqué}"
    blocs_op += "\n"
    blocs_op += r"\blockmove{avancer de \ovalnum{" + str(a) + "}}"
    blocs_op += "\n"
    blocs_op += r"\blockmove{avancer de \ovalnum{" + str(b) + "}}"

    blocs_res = r"\blockinit{quand \greenflag est cliqué}"
    blocs_res += "\n"
    blocs_res += r"\blockmove{avancer de \ovalnum{\ldots}}"

    blocs_corr = r"\blockinit{quand \greenflag est cliqué}"
    blocs_corr += "\n"
    blocs_corr += r"\blockmove{avancer de \ovalnum{" + str(b + a) + "}}"
    return (blocs_op, blocs_res, blocs_corr)


def question_addition(a, b):
    blocs_op, blocs_res, blocs_corr = blocs_addition(a, b)
    return arrange_blocs(blocs_op, blocs_res)


def blocs_multiplication(a, b):
    blocs_op = r"\blockinit{quand \greenflag est cliqué}"
    blocs_op += "\n"
    blocs_op += r"\blockrepeat{répéter \ovalnum{" + str(a) + "} fois}"
    blocs_op += "\n"
    blocs_op += "{\n"
    blocs_op += r"\blockmove{avancer de \ovalnum{" + str(b) + "}}"
    blocs_op += "\n}\n"

    blocs_res = r"\blockinit{quand \greenflag est cliqué}"
    blocs_res += "\n"
    blocs_res += r"\blockmove{avancer de \ovalnum{\ldots}}"

    blocs_corr = r"\blockinit{quand \greenflag est cliqué}"
    blocs_corr += "\n"
    blocs_corr += r"\blockmove{avancer de \ovalnum{" + str(b * a) + "}}"
    
    return (blocs_op, blocs_res, blocs_corr)


def question_multiplication(a, b):
    blocs_op, blocs_res, blocs_corr = blocs_multiplication(a, b)
    return arrange_blocs(blocs_op, blocs_res)