from __future__ import print_function

from sympy import latex, symbols

x = symbols("x")

expression_1 = 2 * x - 4
expression_2 = 5 * x - 5

inequation_1 = expression_1 >= 3
inequation_2 = expression_2 < expression_1
print(latex(x ** 2 + 2 * x - 3))
print(latex(inequation_1.as_set()))
