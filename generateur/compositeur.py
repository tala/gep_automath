import os
import os.path
from os import curdir

import jinja2
from jinja2 import Template

latex_jinja_env = jinja2.Environment(
    block_start_string=r"\BLOCK{",
    block_end_string="}",
    variable_start_string=r"\VAR{",
    variable_end_string="}",
    comment_start_string="\#{",
    comment_end_string="}",
    line_statement_prefix="%%",
    line_comment_prefix="%#",
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(curdir),
)


def generer_interro_flash(niveau, theme, questions, corrections):
    questions_corrigees = [
        dict(enonce=e, correction=c) for e, c in zip(questions, corrections)
    ]
    interro = {
        "niveau": niveau,
        "theme": theme,
        "questions_corrigees": questions_corrigees,
    }
    template = latex_jinja_env.get_template("questionsFlashTPL.tex")
    return template.render(interro=interro)


def generer_grille_automatismes(test):
    template = latex_jinja_env.get_template(
        "grille_automatismes_individualisees_TPL.tex"
    )
    return template.render(test=test)


def generer_corriges_automatismes(test):
    template = latex_jinja_env.get_template(
        "corrigés_automatismes_individualisees_TPL.tex"
    )
    return template.render(test=test)


if __name__ == "__main__":
    from compilateur import compile_recette_1

    niveau = "moyenne section"
    theme = "calculs élémentaires"
    questions = ["machin", "truc", "chouette"]
    corrections = ["machin corrigé", "truc corrigé", "chouette corrigé"]
    print(generer_interro_flash(niveau, theme, questions, corrections))
    compile_recette_1(
        "essai1", generer_interro_flash(niveau, theme, questions, corrections)
    )
