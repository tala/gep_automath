#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from os import chdir, curdir, listdir, mkdir, remove
from os.path import exists, pardir
from platform import system
from subprocess import call

if system() == "Windows":
    from os import startfile


def affiche_pdf(fichier):
    """
    extrait modifié d'actimaths
    """
    # Cas de Windows
    if system() == "Windows":
        startfile(fichier)
    # Cas de Mac OS X
    elif system() == "Darwin":
        call(["open", fichier])
    # Cas de Linux
    elif system() == "Linux":
        call(["xdg-open", fichier])


def compile_recette_1(nom, source):
    try:
        chdir("documents")
    except OSError:
        t, e = sys.exc_info()[:2]
        if e.errno == 2:
            mkdir("documents")
            chdir("documents")
    if exists(nom + ".tex"):
        i = 0
        while exists(nom + "_" + str(i) + ".tex"):
            i = i + 1
        nom = nom + "_" + str(i)
    with open(nom + ".tex", "wb") as fichier:
        fichier.write(source.encode("utf-8"))
    fichier.close()
    for i in range(2):
        call(
            [
                "pdflatex",
                "-shell-escape",
                "-synctex=1",
                "-interaction=nonstopmode",
                fichier.name,
            ]
        )
    prenom = fichier.name.split(".")[0]
    nomPdf = prenom + ".pdf"
    for nom_fichier in listdir(curdir):
        ext = nom_fichier.split(".")[-1]
        if ext not in ["tex", "pdf"]:
            remove(nom_fichier)
    affiche_pdf(nomPdf)
    chdir(pardir)
