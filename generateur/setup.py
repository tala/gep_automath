with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gep_automaths", 
    version="0.0.1",
    author="Nicolas Talabardon",
    author_email="contact@nicolastala.fr",
    description="Un outil d'entraînement sur les automatismes en mathématiques pour l'enseignement secondaire",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://framagit.org/tala/gep_automath/tree/master/generateur",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.4',
)