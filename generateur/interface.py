import tkinter as tk

from compilateur import compile_recette_1
from compositeur import generer_interro_flash

NB_MAX_QUESTION = 20


class FenetrePrincipale(tk.Tk):
    def __init__(self):
        super().__init__()
        super().title("Générateur de questions flash")
        ## ici commence le "plagiat" de la "meilleure réponse"
        ## à https://stackoverflow.com/questions/31762698/dynamic-button-with-scrollbar-in-tkinter-python
        ## la suivante est peut-être plus simple
        ascenceur_vertical = tk.Scrollbar(self, orient=tk.VERTICAL)
        ascenceur_vertical.pack(fill=tk.Y, side=tk.RIGHT, expand=tk.FALSE)
        canvas = tk.Canvas(
            self, bd=0, highlightthickness=0, yscrollcommand=ascenceur_vertical.set
        )
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.TRUE)
        ascenceur_vertical.config(command=canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = tk.Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior, anchor=tk.NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())

        interior.bind("<Configure>", _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())

        canvas.bind("<Configure>", _configure_canvas)

        cadre_niveau = tk.Frame(self.interior)
        etiquette_niveau = tk.Label(cadre_niveau, text="niveau : ")
        self.champ_niveau = tk.Entry(cadre_niveau)
        etiquette_niveau.pack(side=tk.LEFT)
        self.champ_niveau.pack(side=tk.LEFT)
        cadre_niveau.pack()

        cadre_theme = tk.Frame(self.interior)
        etiquette_theme = tk.Label(cadre_theme, text="thème : ")
        self.champ_theme = tk.Entry(cadre_theme)
        etiquette_theme.pack(side=tk.LEFT)
        self.champ_theme.pack(side=tk.LEFT)
        cadre_theme.pack()

        self.champs_questions = []
        self.champs_correction = []
        self.nombre_question = 0

        # barre_menu = tk.Menu(self)
        # barre_menu.add_command(label="Quitter",command=self.destroy)

        # menu = Menu(menubar)

        # entete_fichier = tk.Menubutton(self, text="Fichier")

        self.cadre_questions_et_corriges = tk.Frame(self.interior)
        self.cadre_questions_et_corriges.pack()
        self.ajoute_question()

        cadre_controle = tk.Frame(self.interior)
        bouton_ajouter_question = tk.Button(
            cadre_controle, text="ajouter une question", command=self.ajoute_question
        )
        bouton_generer_test = tk.Button(
            cadre_controle, text="générer le test", command=self.generer_test
        )
        bouton_ajouter_question.pack(side=tk.LEFT)
        bouton_generer_test.pack(side=tk.LEFT)
        cadre_controle.pack()

    def ajoute_question(self):
        if self.nombre_question < NB_MAX_QUESTION:
            question = tk.Frame(self.cadre_questions_et_corriges)
            sous_cadre_question = tk.Frame(question)
            etiquette = tk.Label(
                sous_cadre_question,
                text="question {} :".format(self.nombre_question + 1),
            )
            champ = tk.Text(sous_cadre_question, height=3, width=50)
            etiquette.pack(side=tk.LEFT)
            champ.pack()
            sous_cadre_question.pack()

            question.bouton_ajout_correction = tk.Button(
                question,
                text="corriger",
                command=lambda: self.ajoute_champ_correction(question),
            )

            question.correction = tk.Frame(question)

            etiquette_correction = tk.Label(question.correction, text="correction :")
            champ_correction = tk.Text(question.correction, height=3, width=50)
            etiquette_correction.pack(side=tk.LEFT)
            champ_correction.pack(side=tk.LEFT)

            question.bouton_ajout_correction.pack()
            question.pack()
            self.champs_questions.append(champ)
            self.champs_correction.append(champ_correction)
            self.nombre_question += 1

    def ajoute_champ_correction(self, question):

        question.correction.pack(side=tk.TOP)
        question.bouton_ajout_correction.pack_forget()

    def generer_test(self):
        niveau = self.champ_niveau.get()
        theme = self.champ_theme.get()
        questions = [champ.get("0.1", tk.END)[:-1] for champ in self.champs_questions]
        corrections = [
            champ.get("0.1", tk.END)[:-1] for champ in self.champs_correction
        ]

        compile_recette_1(
            "questions_flash",
            generer_interro_flash(niveau, theme, questions, corrections),
        )


if __name__ == "__main__":
    fenetre_initiale = FenetrePrincipale
(
    fenetre_initiale.mainloop()
