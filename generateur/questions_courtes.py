from csv import DictReader
from random import choice, randint, sample, shuffle

from sympy import Equality, latex, sympify

from compilateur import compile_recette_1
from compositeur import generer_corriges_automatismes, generer_grille_automatismes

## TODO: gestion des fractions
## TODO: intégration avec sympy


def converti_en_string(decimal):
    # je "rattrape" de façon "bourrine" les problèmes d'encodage
    # des floats en binaire... UTILISER LE MODULE DECIMAL OU SYMPY
    arrondi = round(decimal, 3)
    if int(arrondi) == arrondi:
        arrondi = int(arrondi)
    return str(arrondi).replace(".", ",")


def article_associe(verbe):
    if verbe.lower() == "augmenter":
        article = "l'"
    elif verbe.lower() == "diminuer":
        article = "le "
    else:
        ### lever un "avertissement" ?
        article = ""
    return article


def proportions_question_1(pourcentage):
    """Simple question de traduction de pourcentage (positif) en coefficient multiplicateur
    """
    return {
        "enonce": rf"Pour calculer {converti_en_string(pourcentage)} \% d’un nombre, il faut le multiplier par \dots ",
        "reponse": converti_en_string(pourcentage / 100),
    }


def verbe_evolution(pourcentage):
    if pourcentage < 0:
        return "diminuer"
    else:
        return "augmenter"


def proportions_question_2(pourcentage):
    """Simple question de traduction de pourcentage d'évolution en coefficient multiplicateur
    Si le pourcentage est négatif, c'est une diminution, sinon c'est une augmentation
    """
    varier = verbe_evolution(pourcentage)
    return {
        "enonce": rf"{varier.capitalize()} un nombre de {converti_en_string(abs(pourcentage))} \%, revient à le multiplier par \dots ",
        "reponse": converti_en_string(1 + pourcentage / 100),
    }


def proportions_question_3(pourcentage_1, pourcentage_2):
    """Calcul du taux d'évolution équivalent à deux évolutions successives
    """
    varier_1 = verbe_evolution(pourcentage_1)
    varier_2 = verbe_evolution(pourcentage_2)
    article_2 = article_associe(varier_2)
    pourcentage_global = (
        (1 + pourcentage_1 / 100) * (1 + pourcentage_2 / 100) - 1
    ) * 100
    varier_global = verbe_evolution(pourcentage_global)
    article_3 = article_associe(varier_global)
    ## formulation alternative(s) :
    ## Un nombre diminué de 10 % puis de 20 % à été finalement diminué de
    return {
        "enonce": rf"{varier_1.capitalize()} un nombre de {converti_en_string(abs(pourcentage_1))} \% puis {article_2}{varier_2} de {converti_en_string(abs(pourcentage_2))} \%, revient à {article_3}{varier_global} de \dots ",
        "reponse": rf"{converti_en_string(abs(pourcentage_global))} \%",
    }


def proportions_question_4(pourcentage_1, pourcentage_2):
    """Calcul du coefficient multiplicateur équivalent à deux évolutions successives
    """
    varier_1 = verbe_evolution(pourcentage_1)
    varier_2 = verbe_evolution(pourcentage_2)
    article_2 = article_associe(varier_2)
    coeff_global = (1 + pourcentage_1 / 100) * (1 + pourcentage_2 / 100)
    varier_global = verbe_evolution(coeff_global - 1)
    article_3 = article_associe(varier_global)
    ## formulation alternative(s) :
    ## De quel coefficient est multiplié un nombre diminué de 10 % puis de 20 %.
    return {
        "enonce": rf"{varier_1.capitalize()} un nombre de {converti_en_string(abs(pourcentage_1))} \% puis {article_2}{varier_2} de {converti_en_string(abs(pourcentage_2))} \%, revient à le multiplier par \dots ",
        "reponse": rf"{converti_en_string(coeff_global)}",
    }


def presente_point(point):
    return rf"{point[0]}\left({converti_en_string(point[1])}, {converti_en_string(point[2])}\right)"


def droite_question_1(point_1, point_2):
    coefficient = (point_2[2] - point_1[2]) / (point_2[1] - point_1[1])
    return {
        "enonce": f"Quel est le coefficient directeur de la droite qui passe par les points {presente_point(point_1)} et {presente_point(point_2)} ?",
        "reponse": converti_en_string(coefficient),
    }


def fonctions_affines_question_antecedent(fonction, nombre):
    nom_fct, expression_fct = fonction
    expression_fct = sympify(expression_fct)
    equation = Equality(expression_fct, nombre)
    res = list(equation.as_set())[0]
    ## TODO: reprendre le travail fait pour fundamath
    return {
        "enonce": f"Quel est l’antécédent de {converti_en_string(nombre)} par la fonction affine ${nom_fct}$ définie par : ${nom_fct}(x)={latex(expression_fct)}$",
        "reponse": converti_en_string(res),
    }


def fonctions_affines_question_image(fonction, nombre):
    nom_fct, expression_fct = fonction
    expression_fct = sympify(expression_fct)
    res = expression_fct.replace("x", nombre)
    ## TODO: reprendre le travail fait pour fundamath
    return {
        "enonce": f"Quelle est l’image de {converti_en_string(nombre)} par la fonction affine ${nom_fct}$, définie par : ${nom_fct}(x)={latex(expression_fct)}$ ?",
        "reponse": converti_en_string(res),
    }


## TODO: séparer les générateurs aléatoires sous containtes dans un module
##       spécifique


def pourcentage_a_virgule_1():
    return randint(1, 7) + randint(1, 7) / 10


def pourcentage_en_dizaine():
    return randint(1, 9) * 10


def pourcentage_relatif():
    return choice([-1, 1]) * (randint(1, 2) * 10 + randint(1, 9))


def fonction_affine_tres_sympa():
    """!!! Renvoie une chaîne de caractères
    """
    return f"{choice([-1,1])*randint(1,9)}*x + {randint(1,9)}"


def image_par_fct(fct, nombre):
    """
    !!! la fonction est donnée en chaîne de caractère
    """
    return sympify(fct).replace("x", nombre)


with open("listing_1STMG.csv") as fichier_csv:
    lecteur = DictReader(fichier_csv)
    eleves = list(lecteur)


def questionnaire_1(eleve):
    questions = [
        proportions_question_1(pourcentage_a_virgule_1()),
        proportions_question_2(pourcentage_relatif()),
        proportions_question_3(pourcentage_en_dizaine(), pourcentage_en_dizaine()),
        fonctions_affines_question_image(
            [choice("fgh"), fonction_affine_tres_sympa()],
            choice([-1, 1]) * randint(3, 12),
        ),
    ]
    shuffle(questions)
    return {"eleve": eleve, "questions": questions}


tests = {
    "classe": "1 STMG groupe 2",
    "questionnaires": [questionnaire_1(eleve) for eleve in eleves],
}

if __name__ == "__main__":
    # print(proportions_question_1(50))
    # dico = proportions_question_1(4.3)
    # for cle in dico:
    #     print(dico[cle])
    # dico = proportions_question_2(4.3)
    # for cle in dico:
    #     print(dico[cle])
    # dico = proportions_question_3(-20, 30)
    # for cle in dico:
    #     print(dico[cle])
    # dico = droite_question_1(["A", -20, 30], ["B", 10, 20])
    # for cle in dico:
    #     print(dico[cle])
    # dico = fonctions_affines_question_antecedent(["f", "2*x-5"], 10)
    # for cle in dico:
    #     print(dico[cle])
    # dico = fonctions_affines_question_image(["f", "2*x-5"], 10)
    # for cle in dico:
    #     print(dico[cle])

    # print(tests)
    # compile_recette_1("test_automatismes", generer_grille_automatismes(tests))
    # compile_recette_1("test_automatismes_corrigé", generer_corriges_automatismes(tests))

    questions_Anais = [
        proportions_question_1(pourcentage_a_virgule_1()),
        proportions_question_2(pourcentage_relatif()),
        proportions_question_4(pourcentage_en_dizaine(), pourcentage_en_dizaine()),
        fonctions_affines_question_image(
            [choice("fgh"), fonction_affine_tres_sympa()],
            choice([-1, 1]) * randint(3, 12),
        ),
        proportions_question_1(pourcentage_a_virgule_1()),
        proportions_question_2(pourcentage_relatif()),
        proportions_question_4(pourcentage_en_dizaine(), pourcentage_en_dizaine()),
        fonctions_affines_question_image(
            [choice("fgh"), fonction_affine_tres_sympa()],
            choice([-1, 1]) * randint(3, 12),
        ),
    ]
    shuffle(questions_Anais)

    punition = {
        "classe": "1 STMG groupe 2",
        "questionnaires": [{"eleve": eleves[0], "questions": questions_Anais}],
    }

    compile_recette_1("punition_Anais", generer_grille_automatismes(punition))
    compile_recette_1("punition_Anais", generer_corriges_automatismes(punition))
