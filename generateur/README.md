# Générateur de questions flash
## objectif
Le but de ce programme est de permettre à un utilisateur, ayant peu de connaissance sur le langage d'édition Latex, de générer, sans trop de difficultés, des diaporamas pour interroger ses élèves, afin de tester des automatismes nécessaires en classe de mathématiques.
Dans l’état actuel de son développement, il est nécessaire de saisir manuellement les questions et leurs éventuelles corrections. Pour cela quelques bagages de langage Latex sont nécessaires.
## Installation

Les instructions suivantes ont été testées sur windows 10 et Ubuntu 18.04.
### Prérequis
Pour utiliser le générateur de diaporamas, il est nécessaire d’installer sur votre poste :  
* python dans sa version 3.6 ou une version plus récente
* une distribution Latex (miktex ou texlive par exemple)
* jinja2 : http://jinja.pocoo.org/docs/2.10/, la librairie qui permet de "remplir" un document latex avec des données en mémoire. 
A priori, une des deux commandes suivantes devrait faire l'affaire : "pip install Jinja2" ou, à défaut : "easy_install Jinja2".
### Installation du programme lui-même
Une fois ces prérequis complétés, il faut télécharger l'archive "générateur.zip" et extraire son contenu dans un répertoire "générateur".
## Utilisation
Une fois les fichiers extraits, rendez-vous dans le répertoire créé.
Le fichier à lancer est « interface.py », soit en saisissant la commande « python interface.py » , soit en ouvrant le fichier avec un éditeur de code et en lançant le module.
Une fois les champs saisis et la commande de génération exécutée, le diaporama doit s'afficher automatiquement. Les documents « .tex » et « .pdf » sont créés dans un répertoire « documents ».

## fonctionnement interne / développement
![](fonctionnement.png)

les fichiers constituant se programme sont publiés, dans leurs versions les plus récentes sur :

https://framagit.org/tala/gep_automath/tree/master/generateur

## feuille de route